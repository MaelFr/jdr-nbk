import React from 'react';
import { UserConsumer } from '../components/UserContext';
import Layout from '../components/Layout';

function Home() {
  return (
    <Layout>
      <UserConsumer>
        {({ user }) => <div>Welcome{user && ` ${user.name}`}</div>}
      </UserConsumer>
    </Layout>
  );
}

export default Home;
