import React from 'react';
import Layout from '../components/Layout';
import Login from '../components/Login';
import { UserConsumer } from '../components/UserContext';

function LoginPage() {
  return (
    <Layout title="Login">
      <UserConsumer>
        {({ updateUser }) => <Login updateUser={updateUser} />}
      </UserConsumer>
    </Layout>
  );
}

export default LoginPage;
