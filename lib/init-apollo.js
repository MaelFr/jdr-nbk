import { ApolloClient, InMemoryCache, HttpLink } from 'apollo-boost';
import fetch from 'isomorphic-unfetch';

let apolloClient = null;

// Polyfill fetch() sur le serveur
if (!process.browser) {
  global.fetch = fetch;
}

function create(initialState) {
  return new ApolloClient({
    connectToDevTools: process.browser,
    ssrMode: !process.browser, // Désactive forceFetch sur le serveur (permet aux requêtes de n'être lancées qu'une fois)
    link: new HttpLink({
      uri: process.env.GRAPHQL_SERVER,
      credentials: 'same-origin'
    }),
    cache: new InMemoryCache().restore(initialState || {})
  });
}

export default function initApollo(initialState) {
  // Créé un nouveau client par requête pour éviter que les données soient partagées
  if (!process.browser) {
    return create(initialState);
  }

  // Réutilise le client Apollo si on est sur un navigateur
  if (!apolloClient) {
    apolloClient = create(initialState);
  }

  return apolloClient;
}
