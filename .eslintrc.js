module.exports = {
  extends: ['airbnb', 'prettier', 'prettier/react'],
  rules: {
    'no-console': ['error', { allow: ['warn', 'error'] }],
    'jsx-a11y/anchor-is-valid': 'off',
  },
};
