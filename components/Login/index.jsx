import React, { useState } from 'react';
import { Mutation } from 'react-apollo';
import gql from 'graphql-tag';
import Router from 'next/router';
import PropTypes from 'prop-types';
import { AUTH_TOKEN } from '../../contants';

const authFragment = gql`
  fragment authFragment on AuthPayload {
    token
    user {
      name
      email
    }
  }
`;

const SIGNUP_MUTATION = gql`
  mutation SignUpMutation($email: String!, $password: String!, $name: String!) {
    signUp(email: $email, password: $password, name: $name) {
      ...authFragment
    }
  }
  ${authFragment}
`;
const LOGIN_MUTATION = gql`
  mutation LoginMutation($email: String!, $password: String!) {
    login(email: $email, password: $password) {
      ...authFragment
    }
  }
  ${authFragment}
`;

function Login({ updateUser }) {
  const [login, setLogin] = useState(true);
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  async function confirm(data) {
    const { token, user } = login ? data.login : data.signup;
    sessionStorage.setItem(AUTH_TOKEN, token);
    updateUser(user);
    Router.push('/');
  }

  return (
    <form>
      <h2>{login ? 'Login' : 'Sign Up'}</h2>
      <div>
        {!login && (
          <input
            value={name}
            onChange={e => setName(e.target.value)}
            type="text"
            placeholder="Your name"
          />
        )}
        <input
          value={email}
          onChange={e => setEmail(e.target.value)}
          type="text"
          placeholder={login ? 'Email address' : 'Your email address'}
        />
        <input
          value={password}
          onChange={e => setPassword(e.target.value)}
          type="password"
          placeholder={login ? 'Password' : 'Choose a safe password'}
        />
      </div>
      <div>
        <Mutation
          mutation={login ? LOGIN_MUTATION : SIGNUP_MUTATION}
          variables={{ name, email, password }}
          onCompleted={confirm}
        >
          {mutation => (
            <button
              type="submit"
              onClick={event => {
                event.preventDefault();
                mutation();
              }}
            >
              {login ? 'login' : 'create account'}
            </button>
          )}
        </Mutation>
        <button type="button" onClick={() => setLogin(!login)}>
          {login ? 'need to create an account?' : 'already have an account?'}
        </button>
      </div>
    </form>
  );
}
Login.propTypes = {
  updateUser: PropTypes.func.isRequired,
};

export default Login;
