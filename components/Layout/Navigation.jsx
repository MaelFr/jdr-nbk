import React from 'react';
import Link from 'next/link';
import { UserConsumer } from '../UserContext';

function Navigation() {
  return (
    <UserConsumer>
      {({ user, updateUser }) => (
        <nav>
          <Link href="/">
            <a>Home</a>
          </Link>
          {' | '}
          {user ? (
            <button
              type="button"
              onClick={() => {
                updateUser(null);
              }}
            >
              Logout
            </button>
          ) : (
            <Link href="/login">
              <a>Login</a>
            </Link>
          )}
        </nav>
      )}
    </UserConsumer>
  );
}

export default Navigation;
