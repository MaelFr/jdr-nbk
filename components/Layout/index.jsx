import React from 'react';
import Head from 'next/head';
import PropTypes from 'prop-types';
import Navigation from './Navigation';

function Layout({ children, title }) {
  return (
    <>
      <Head>
        <title>{title}</title>
        <meta charSet="utf-8" />
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
      <header>
        <Navigation />
      </header>

      {children}

      <footer>Footer</footer>
    </>
  );
}

Layout.propTypes = {
  title: PropTypes.string,
  children: PropTypes.node.isRequired,
};

Layout.defaultProps = {
  title: 'MyApp',
};

export default Layout;
