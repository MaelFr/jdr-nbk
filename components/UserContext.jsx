import React, { useState } from 'react';
import PropTypes from 'prop-types';

const UserContext = React.createContext({
  user: null,
  updateUser: () => {},
});

const UserConsumer = UserContext.Consumer;

function UserProvider({ children }) {
  const [user, updateUser] = useState(null);

  return (
    <UserContext.Provider value={{ user, updateUser }}>
      {children}
    </UserContext.Provider>
  );
}
UserProvider.propTypes = {
  children: PropTypes.node.isRequired,
};

export { UserProvider, UserConsumer };
