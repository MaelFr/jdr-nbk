const mongoose = require('mongoose');

mongoose.Promise = global.Promise;

const url = process.env.MONGODB_URL;

mongoose.connect(url, { useNewUrlParser: true });
mongoose.connection.once('open', () => {
  if (process.env.NODE_ENV !== 'production') {
    // eslint-disable-next-line no-console
    console.log(`Connected to mongo at ${url}`);
  }
});
