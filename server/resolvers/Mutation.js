const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const User = require('../models/userModel');

const saltRounds = 12;

async function signUp(parent, args) {
  const password = await bcrypt.hash(args.password, saltRounds);
  const user = await User.create({ ...args, password });
  const token = jwt.sign({ userId: user.id }, process.env.APP_SECRET);

  return {
    token,
    user,
  };
}
async function login(parent, args) {
  const user = await User.findOne({ email: args.email }).exec();
  if (!user) {
    throw new Error('Invalid email/password');
  }
  const valid = await bcrypt.compare(args.password, user.password);
  if (!valid) {
    throw new Error('Invalid email/password');
  }
  const token = jwt.sign({ userId: user.id }, process.env.APP_SECRET);

  return {
    token,
    user,
  };
}

module.exports = {
  signUp,
  login,
};
