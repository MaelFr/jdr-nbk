const User = require('../models/userModel');

async function getUsers() {
  return User.find().exec;
}
async function getUser(parent, { id }) {
  return User.findById(id).exec();
}

module.exports = {
  getUsers,
  getUser,
};
